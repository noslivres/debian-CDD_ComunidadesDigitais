# Debian-CDD Comunidades Digitais

Debian-CDD Comunidades Digitais é uma Debian Custom CD voltada para automatização da instalação, uso e manutenção de serviços e aplicações essenciais para Provedores Comunitários de Redes.
A distribuição automatiza o processo de instalação com as opções básicas mais comuns de instalação e oferece módulos com aplicações e serviços básicos disponibilizados no formato de containers em Docker. Toda as instalação básica não depende de conexão com a internet.


Debian-CDD Comunidades Digitais faz parte do projeto de Redes Lives e Cidades Digitais do Instituto Federal de Campos de Goycatazes/RJ em parceria com IBEBrasil e Nós Livres.

## Serviços disponíveis até o momento:

- Portal Comunitário (Wordpress + Boddypress)
- Plataforma de EaD (Moodle)
- Sistema de Gestão (Odoo)

## Serviços previstos em novas versões:

- Plataforma de participação social (Cidade Democrática)
- Rede social federada (Diaspora)
- Servidor de streaming
- Servidor de compartilhamento de arquivos (OwnCloud)


## Download
http://files.thiagopaixao.com/debian-9.1.0-amd64-Comunidades_Digitais-DVD.iso

Para gravar a imagem ISO em um USB bootável, usar o aplicativo Unetbootin ou o comando "dd", ambos com opção bs = 2048:

dd if=/pasta/debian-9.1.0-amd64-Comunidades_Digitais-DVD.iso bs=2048 of=/dev/sdx 

(substituir sdx por pelo dispositivo do pendrive, que geralmente é sdb)


## Instruções básicas

### Instalação
Ao realizar a instalação, o pós-script irá realizar a cópia dos arquivos e scripts de customização no termino da instalação. No primeiro boot, o script "fristboot" é invocado, realizando a instalação do Docker e dos containers das aplicações e serviços. Ao finalizar, o firstboot reiiciará automáticamente do computador e então estará pronto para uso.

### Rede
Na atual versão, os serviços só funcionam se o IP do servidor estiver configurado como "192.168.1.43/24", por isso a instalação configura esse IP fixo como default. Para correto funcionamento dos serviços, este IP não deve ser auterado. Está previsto no roadmap do projeto, deixar dinâmico a alteração do IP nos serviços.

### Administração
Por padrão apenas apenas o serviço do Portal Comunitário inicializará automaticamente na inicialização do sistema após a instalação. Os demais serviços devem ser inicializados manualmente conforma a necessidade, e poderam serem configurados para inicializar com o sistema também.

- Para inicializar um serviço com saída de logs no terminal:
/opt/dockers/<serviço>/start.sh


- Para inicializar um serviço em background:
systemctl start <serviço>


- Para habilitar serviço na inicialização do sistema:
systemctl enable <serviço>


- Para remover serviço da inicialização do sistema:
systemctl disable <serviço>

### Acesso
O sistema vem com as seguintes senhas padrões que devem ser alteradas após a instalação:

Usuário: root
Senha: r00tme

Usuário: administrador
Senha: r00tme

### Serviços e Aplicações
Todos os serviços estão no formato de Docker-Composer, e estão armazenados em "/opt/dockers".

- Portal Comunitário (Wordpress)
http://192.168.1.43:80
Painel: http://192.168.1.43:80/admin
Usuário: admin
Senha: 123456
Caminho: /opt/dockers/portal-cd/
Serviço: portal-cd


- Plataforma de EaD (Moodle)
http://192.168.1.43:8080
Painel: http://192.168.1.43:8080/admin
Usuário: admin
Senha: 123456
Caminho: /opt/dockers/moodle-cd/
Serviço: moodle-cd


- Sistema de Gestão (Odoo)
http://192.168.1.43:8069
Usuário: sgpadmin@comunidadesdigitais.org.br
Senha: 123456
Caminho: /opt/dockers/sgp-cd/
Serviço: sgp-cd
