#!/bin/bash
cd irmod 
find . | cpio -H newc --create --verbose | gzip -9 > ../cd/install.amd/initrd.gz

cd ../cd
md5sum `find -follow -type f` > md5sum.txt

cd ..
genisoimage -o test.iso -r -J -no-emul-boot -boot-load-size 4  -boot-info-table -b isolinux/isolinux.bin -c isolinux/boot.cat ./cd

